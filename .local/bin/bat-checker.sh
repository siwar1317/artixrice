#!/bin/sh

battery_level=$(cat /sys/class/power_supply/BAT0/capacity)
state=$(cat /sys/class/power_supply/BAT0/status)

if [ "$battery_level" -le 15 ] && [ "$state" = "Discharging" ]
then
	notify-send -u critical "LOW BATTERY" "Connect to AC"
elif [ "$battery_level" -gt 95 ] && [ "$state" = "Charging" ]
then
	notify-send "BATTERY CHARGED" "Disconnect from AC"
fi
