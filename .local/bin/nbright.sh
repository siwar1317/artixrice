#!/usr/bin/sh

notify-send "Brightness level" "$(xbacklight -get | sed 's/[.][0-9][0-9][0-9][0-9][0-9][0-9]//g')%"
