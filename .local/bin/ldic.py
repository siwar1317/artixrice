#!/usr/bin/python3
# linksDic.py - saves important links in a dictionary object.

import os
import sys
import shelve
import pyperclip

#if not os.path.dirname(sys.argv[0]) == '':
#    os.chdir('/home/siwar/Documents/python/own')

with shelve.open('/home/siwar/.local/share/ldic') as File:
    if len(sys.argv) == 2 and sys.argv[1] != "list":
        links = File['links']
        print(links.get(sys.argv[1], 0))
        pyperclip.copy(links.get(sys.argv[1], 0))
    elif len(sys.argv) == 3:
        links = File['links']
        links.setdefault(sys.argv[1], sys.argv[2])
        File['links'] = links
        print('Link added.')
    elif sys.argv[1] == 'list':
        links = File['links']
        print(dict.keys(links))
