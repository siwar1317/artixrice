#! /usr/bin/python3
# fast.py - creates a python file with the shebang and the first line comment.

import os
import sys

# checking if the extension specified is ".py"
if not sys.argv[1].endswith('.py'):
    raise Exception('File extension must be ".py".')

# create folder if folder specified doesn't exists.
if not os.path.dirname(sys.argv[1]) == '':
    os.makedirs(os.path.dirname(sys.argv[1]), exist_ok=True)

# create the .py file with the shebang.
pyFile = open(sys.argv[1], 'w')
pyFile.write('#! /usr/bin/python3\n# ' +
             os.path.basename(sys.argv[1]) + ' - ')
pyFile.close()

print(sys.argv[1] + ' was created')
