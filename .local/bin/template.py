#! /usr/bin/python3
# template.py - creates different document types with basic contents.
# USAGE:template.py <doc_type> <file_path> [en]

import os
import sys

# check if extension specified is ".tex" or ".html"
#if not sys.argv[2].endswith('.tex') or not sys.argv[2].endswith('.html'):
#    raise Exception('File extension must be ".tex" or ".html".')

# create folder if folder specified doesn't exist.
os.makedirs(os.path.dirname(sys.argv[2]), exist_ok=True)

# open and read template depending on which type of document and language is specified.
if len(sys.argv) == 4:
    if sys.argv[1] == 'beamer':
        t = open('/home/siwar/.local/share/template/beamer_template_en.tex')
    if sys.argv[1] == 'pdf':
        t = open('/home/siwar/.local/share/template/template_en.tex')
    if sys.argv[1] == 'html':
        t = open('/home/siwar/.local/share/template/plantilla_web_en.html')
    template = t.read()
    t.close()
else:
    if sys.argv[1] == 'beamer':
        t = open('/home/siwar/.local/share/template/beamer_template_es.tex')
    if sys.argv[1] == 'pdf':
        t = open('/home/siwar/.local/share/template/template_es.tex')
    if sys.argv[1] == 'html':
        t = open('/home/siwar/.local/share/template/plantilla_web.html')
    if sys.argv[1] == 'rmd':
        t = open('/home/siwar/.local/share/template/Rmd_template.rmd')
    template = t.read()
    t.close()

# write contents of template to desired ".tex" or ".html" file.
File = open(sys.argv[2], 'w')
File.write(template)
File.close()

print(sys.argv[2] + ' was created.')
