let mapleader =","

call plug#begin('~/.local/share/nvim/plugged')

Plug 'lervag/vimtex'
Plug 'bling/vim-airline'
Plug 'jalvesaq/Nvim-R', {'branch': 'stable'}
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set number relativenumber
set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.
set clipboard=unnamedplus
set splitbelow splitright

" Setting for vimtex
let g:tex_flavor = 'latex'

" settings for zathura
let g:vimtex_view_general_viewer = 'zathura'

" settings for nvim-R
let R_assign_map = ''

" keymaps
imap <C-p> <p></p><Esc>3hi
imap <C-i> <i></i><Esc>3hi
imap <C-s> <strong></strong><Esc>8hi
imap <C-h> <h2></h2><Esc>4hi
imap <C-n> \begin{}<Enter>\end{}<Esc>k2li
imap <C-a> \textit{}<Esc>i
imap <C-z> \textbf{}<Esc>i
imap <C-v> \vspace{0.3cm}<Esc>
imap <C-V> \vspace{0.5cm}<Esc>
map <C-u> :PlugUpdate<CR>
map <W> :w<CR>

" Shortcutting tab navigation
nnoremap tn :tabnew<Space>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprev<CR>
nnoremap th :tabfirst<CR>
nnoremap tl :tablast<CR>

" Shorcut to open a split
map <M-E> :vsp<Space>

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Sourcing the coc config
source $HOME/.config/nvim/plug-config/coc.vim
